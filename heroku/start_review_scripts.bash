#!/bin/bash -ex

PROJECT_NAME='students'
# Currently GitLab CI jobs run on the forked project,
# so when getting $CI_PROJECT_NAMESPACE and $CI_PROJECT_ID
# it returns the namespace and project ID of the forked and not the parent project
# Declaring a variable with the same name as one of the predefined variables of
# Gitlab CI overrides the value of the predefined one and uses the value of the
# variable declared in this script.
CI_PROJECT_ID='9283876'
CI_PROJECT_NAMESPACE='universis'

curl -X POST https://api.heroku.com/apps -H "Accept: application/vnd.heroku+json; version=3" -H "Authorization: Bearer $HEROKU_API_KEY" -H "Content-Type: application/json" -d "{\"name\":\"$PROJECT_NAME-$CI_MERGE_REQUEST_IID\",\"region\":\"eu\"}"

git checkout -B $PROJECT_NAME-$CI_MERGE_REQUEST_IID
git status
git config user.email 'no-reply@universis.io'
git config user.name 'UniverSIS Project'

sed -i -e "s/\"callbackURL.*,/\"callbackURL\": \"https:\/\/$PROJECT_NAME-$CI_MERGE_REQUEST_IID.herokuapp.com\/auth\/callback\/index.html\",/" src/assets/config/app.json

sed -i -e "s/\"logoutURL.*,/\"logoutURL\":\"https:\/\/users.universis.io\/logout?continue=https:\/\/$PROJECT_NAME-$CI_MERGE_REQUEST_IID.herokuapp.com\/#\/auth\/login\",/" src/assets/config/app.json

sed -i -e "s/\"clientID.*,/\"clientID\": \"$CLIENT_ID\",/"  src/assets/config/app.json

# Uncomment the script when deploying to heroku and replace
# hardcoded variables with their corresponding value each time
sed -i -e "s/<\!--<script defer/<script defer/" src/index.html
sed -i -e "s/<\/script>-->/<\/script>/" src/index.html
sed -i -e "s/data-project-id.*/data-project-id=\"$CI_PROJECT_ID\"/" src/index.html
sed -i -e "s/data-merge-request-id.*/data-merge-request-id= \"$CI_MERGE_REQUEST_IID\"/" src/index.html
sed -i -e "s/data-project-path.*/data-project-path=\"$CI_PROJECT_NAMESPACE\/$CI_PROJECT_NAME\"/" src/index.html

cp src/assets/config/app.json src/assets/config/app.production.json
cp src/assets/config/app.production.json src/assets/config/app.development.json
git status
git add -f src/assets/config/app.production.json
git add .
git commit -m "Ready to push latest changes to new heroku review app"
git push -f https://heroku:$HEROKU_API_KEY@git.heroku.com/$PROJECT_NAME-$CI_MERGE_REQUEST_IID.git $PROJECT_NAME-$CI_MERGE_REQUEST_IID:master
