import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../../../profile/services/profile.service';
import {GradeAverageResult, GradesService, CourseGradeBase} from '../../../grades/services/grades.service';
import {TranslateService} from '@ngx-translate/core';
import {GradeScaleService, SemesterPipe} from '@universis/common';
import {GradeScale} from '@universis/common';

@Component({
  selector: 'app-progress-bar-degree',
  templateUrl: './progress-bar-degree.component.html',
  styleUrls: ['./progress-bar-degree.component.scss']
})
export class ProgressBarDegreeComponent implements OnInit {

  public currentIndex?: number;
  public toShowSmallDots = true;
  public student: any;
  public isLoading = true;   // Only if data is loaded
  public weightedGradesAverage: GradeAverageResult = {
    courses: 0,
    passed: 0,
    grades: 0,
    coefficients: 0,
    average: 0,
    ects: 0,
    units: 0
  };
  public simpleGradesAverage: GradeAverageResult = this.weightedGradesAverage;

  public SemesterPoints: any[] = [
    {number: 1, active: false, complete: false, passed: 0, weightedAverageGrade: 0.0, simpleAverageGrade: 0.0, indicator: ''},
    {number: 2, active: false, complete: false, passed: 0, weightedAverageGrade: 0.0, simpleAverageGrade: 0.0, indicator: ''},
    {number: 3, active: false, complete: false, passed: 0, weightedAverageGrade: 0.0, simpleAverageGrade: 0.0, indicator: ''},
    {number: 4, active: false, complete: false, passed: 0, weightedAverageGrade: 0.0, simpleAverageGrade: 0.0, indicator: ''},
    {number: 5, active: false, complete: false, passed: 0, weightedAverageGrade: 0.0, simpleAverageGrade: 0.0, indicator: ''},
  ];
  public defaultGradeScale?: GradeScale;

  constructor(private _profileService: ProfileService,
              private _gradesService: GradesService,
              private _gradeScaleService: GradeScaleService,
              private _translateService: TranslateService) {
  }

  ngOnInit() {
    this._profileService.getStudent().then(res => {

      const student = res; // Load data
      this.student = res;
      // check if current semester is greater than max semesters of studyProgram
      if (student.studyProgram.semesters < 4) {
        this.toShowSmallDots = false;
      }

      this._gradesService.getGradeInfo().then((grades) => {
        const allGrades = grades.map(grade => {
          return <CourseGradeBase> grade;
        });
        this.simpleGradesAverage = this._gradesService.getGradesSimpleAverage(allGrades);
        this.weightedGradesAverage = this._gradesService.getGradesWeightedAverage(allGrades);
        if (!this.defaultGradeScale) {

          // get default grade scale
          this._gradesService.getDefaultGradeScale().then(gradeScale => {
            this.defaultGradeScale = gradeScale;
            this.fillData(student, this.weightedGradesAverage, this.simpleGradesAverage);
            this.isLoading = false; // Data is loaded
          });
        } else {
          this.fillData(student, this.weightedGradesAverage, this.simpleGradesAverage);
          this.isLoading = false; // Data is loaded
        }
      });
    });
  }

  fillData(student: any, weightedGradesAverage: GradeAverageResult, simpleGradesAverage: GradeAverageResult) {

    if (student.studentStatus.alternateName === 'graduated' || student.studentStatus.alternateName === 'declared') { // if the student has graduated

      const studentSemester = student.semester;

      // set numbers and states for others points
      for (let i = 0; i < 5; i++) {
        this.SemesterPoints[i].complete = true; // previous semesters
        this.SemesterPoints[i].number = (studentSemester + i) - 3;
        this.SemesterPoints[i].indicator = new SemesterPipe(this._translateService).transform(this.SemesterPoints[i].number, 'medium');
      }
      this.currentIndex = 4;
      this.SemesterPoints[4].passed = weightedGradesAverage.passed;
      this.SemesterPoints[4].weightedAverageGrade = student.graduationGradeAdjusted;
      this.SemesterPoints[4].active = true;
      if (student.studentStatus.alternateName === 'declared') {
        this.SemesterPoints[4].complete = false;
      }
    } else if (student.semester >= 1 && student.semester <= 4) { // if the student is from 1st to 4th semester
      const studentSemester = student.semester;

      // set state for others points
      for (let i = 0; i < 4; i++) {
        if (i < studentSemester - 1) {
          this.SemesterPoints[i].complete = true;
        }
        this.SemesterPoints[i].indicator = new SemesterPipe(this._translateService).transform(this.SemesterPoints[i].number, 'medium');
      }

      this.currentIndex = studentSemester - 1;
      // current semester
      this.SemesterPoints[studentSemester - 1].active = true;
      this.SemesterPoints[studentSemester - 1].number = studentSemester;
      this.SemesterPoints[studentSemester - 1].passed = weightedGradesAverage.passed;
      this.SemesterPoints[studentSemester - 1].weightedAverageGrade = this.defaultGradeScale?.format(weightedGradesAverage.average);
      this.SemesterPoints[studentSemester - 1].simpleAverageGrade = this.defaultGradeScale?.format(simpleGradesAverage.average);
      this.SemesterPoints[studentSemester - 1].indicator = new SemesterPipe(this._translateService).transform(this.SemesterPoints[studentSemester - 1].number, 'medium');
    } else if (student.semester > 4) { // if the student is in a semester higher than the 4th

      const studentSemester = student.semester;

      // set numbers and states for others points
      for (let i = 0; i < 3; i++) {
        this.SemesterPoints[i].complete = true; // previous semesters
        this.SemesterPoints[i].number = (studentSemester + i) - 3;
        this.SemesterPoints[i].indicator = new SemesterPipe(this._translateService).transform(this.SemesterPoints[i].number, 'medium');
      }

      this.currentIndex = 3;
      // current semester
      this.SemesterPoints[3].number = student.semester;
      this.SemesterPoints[3].active = true;
      this.SemesterPoints[3].passed = weightedGradesAverage.passed;
      this.SemesterPoints[3].weightedAverageGrade = this.defaultGradeScale?.format(weightedGradesAverage.average);
      this.SemesterPoints[3].simpleAverageGrade = this.defaultGradeScale?.format(simpleGradesAverage.average);
      this.SemesterPoints[3].indicator = new SemesterPipe(this._translateService).transform(this.SemesterPoints[3].number, 'medium');
    } else if (student.semester === 0) {
      for (let i = 0; i < 4; i++) {
        this.SemesterPoints[i].indicator = new SemesterPipe(this._translateService).transform(this.SemesterPoints[i].number, 'medium');
      }
    }
  }
}
