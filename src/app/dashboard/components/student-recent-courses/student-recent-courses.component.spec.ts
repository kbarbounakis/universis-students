import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ConfigurationService, SharedModule} from '@universis/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CurrentRegistrationService} from '../../../registrations/services/currentRegistrationService.service';
import {StudentRecentCoursesComponent} from './student-recent-courses.component';
import {TestingConfigurationService} from '../../../test';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ProfileService} from '../../../profile/services/profile.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('StudentRecentCoursesComponent', () => {
    let component: StudentRecentCoursesComponent;
    let fixture: ComponentFixture<StudentRecentCoursesComponent>;
    const currentReg = jasmine.createSpyObj('CurrentRegistrationService', ['getCurrentRegistration', 'getCurrentRegistrationEffectiveStatus'] );
    const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
    currentReg.getCurrentRegistration.and.returnValue(Promise.resolve(JSON.parse('{"value":[]}')));
    currentReg.getCurrentRegistrationEffectiveStatus.and.returnValue(Promise.resolve(JSON.parse('{"code":"CLOSED_REGISTRATION","status":"closed","name":"Η δήλωση μαθημάτων δεν μπορεί να τροποποιηθεί."}')));
    profileSvc.getStudent.and.returnValue(Promise.resolve(JSON.parse('{"id":20087281,"department":{"organization":{"instituteConfiguration":{"courseClassUrlTemplate":"https://qa.auth.gr/el/class/1/${id}","eLearningUrlTemplate":"https://qa.auth.gr/el/class/1/${id}/elearning"}}}}')));
beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [StudentRecentCoursesComponent],
            imports: [TranslateModule.forRoot(),
                HttpClientTestingModule,
                TooltipModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                RouterTestingModule,
              SharedModule
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                {
                  provide: CurrentRegistrationService,
                  useValue: currentReg
                },
                {
                  provide: ProfileService,
                  useValue: profileSvc
                }
                ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentRecentCoursesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
