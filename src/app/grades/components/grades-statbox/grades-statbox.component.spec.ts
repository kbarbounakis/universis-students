import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigurationService } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';
import { GradesService } from '../../services/grades.service';
import { GradeScaleService } from '@universis/common';
import {NgChartsModule} from 'ng2-charts';
import { GradesStatboxComponent } from './grades-statbox.component';
import {TestingConfigurationService} from '../../../test';
import {MostModule} from '@themost/angular';
import {BrowserModule} from '@angular/platform-browser';
import {GradesModule} from '../../grades.module';

describe('GradesStatboxComponent', () => {
  let component: GradesStatboxComponent;
  let fixture: ComponentFixture<GradesStatboxComponent>;

  const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
                                        'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod', 'getRecentGrades',
                                        'getGradesSimpleAverage', 'getGradesWeightedAverage']);
  const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ GradesStatboxComponent ],
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        BrowserModule,
        NgChartsModule,
        MostModule.forRoot({
            base: '/',
            options: {
                useMediaTypeExtensions: false
            }
        })],
      providers: [
          {
              provide: GradesService,
              useValue: gradeSvc
          },
          {
              provide: GradeScaleService,
              useValue: gradeScaleSvc
          },
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesStatboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
