import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { DepartmentInfoComponent } from './department-info.component';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { LoadingService } from '@universis/common';
import { NgPipesModule } from "ngx-pipes";

describe('DepartmentInfoComponent', () => {
  let component: DepartmentInfoComponent;
  let fixture: ComponentFixture<DepartmentInfoComponent>;

  const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading' , 'hideLoading' ]);

  profileSvc.getStudent.and.returnValue(Promise.resolve(JSON.parse( '{"department":{ "id": 424, "organization": 111, "name": "ΜΗΧΑΝΙΚΩΝ ΠΕΡΙΒΑΛΛΟΝΤΟΣ", "abbreviation": "ΜΗΧ-ΠΕΡ", "city": "ΞΑΝΘΗ", "address": "Βασιλίσσης Σοφίας 12", "postalCode": "67100", "country": "ΕΛΛΑΔΑ", "alternativeCode": "476", "currentYear": 2017, "currentPeriod": 2, "facultyName": "ΠΟΛΥΤΕΧΝΙΚΗ ΣΧΟΛΗ", "phone1": "+302541079109", "phone2": "+302541079108", "studyLevel": 1, "contactPerson1": null, "contactPerson2": null, "email": "secr@env.duth.gr", "totalSemesters": 10, "url": "www.eng.duth.gr", "localDepartment": 1, "dateModified": "2018-02-26T09:19:20.170Z" } }' )));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NgPipesModule ],
      declarations: [ DepartmentInfoComponent ],
      providers: [
        {
          provide: ProfileService,
          useValue: profileSvc
        },
        {
          provide: LoadingService,
          useValue: loadingSvc
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
