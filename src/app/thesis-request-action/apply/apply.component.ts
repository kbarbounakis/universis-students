import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import {ErrorService, ModalService, ToastService, DIALOG_BUTTONS, LoadingService, ConfigurationService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { cloneDeep } from 'lodash';
import { DatePipe } from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {HttpClient} from '@angular/common/http';
import {Converter} from 'showdown';
import {RequestsService} from '../../requests/services/requests.service';
import {AdvancedFormComponent} from '@universis/forms';


@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html'
})
export class ApplyComponent implements OnInit {

 // public data: any;
  public department: any;
  public student: any;
  public loading = true;
  public src = 'ThesisRequestActions/new';
  public continueLink = [ '/requests', 'list' ];
  public data = {};
  public thesisRequestAction: any;

  @ViewChild('agree') agree;
  @ViewChild('form') form?: AdvancedFormComponent;
  constructor(private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _router: Router,
              private  _toastService: ToastrService,
    private _loadingService: LoadingService,
    private _profileService: ProfileService,
              private _http: HttpClient,
              private _configurationService: ConfigurationService,
              private _activatedRoute: ActivatedRoute,
              private _requestsService: RequestsService ) { }

    showLoading(loading: boolean) {
      this.loading = loading;
      if (loading) {
        this._loadingService.showLoading();
      } else {
        this._loadingService.hideLoading();
      }
    }

  async ngOnInit() {
    const student = await this._profileService.getStudent();
    this.showLoading(true);
     this.thesisRequestAction = await this._context.model('StudentRequestActions')
      .asQueryable()
      .where('additionalType').equal('ThesisRequestAction')
      .and('actionStatus/alternateName').equal('ActiveActionStatus')
      .orderByDescending('dateCreated')
      .take(-1)
      .getItem();
    if (this.thesisRequestAction != null) {
      return this._router.navigate(['/requests', 'ThesisRequestActions', this.thesisRequestAction.id, 'preview']);
    } else  {
      Object.assign(this.data,
        {
          academicYear: student.department.currentYear,
          // tslint:disable-next-line:max-line-length
          academicPeriod: student.department.currentPeriod,
          student: student.id,
          name: this._translateService.instant('ThesisRequestActions.ThesisRequest')
        });
    this.showLoading(false);
    }
  }

  async onCompletedSubmission($event: any) {
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map(x => {
        return x;
      });
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    } else {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute});
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    }
  }
}
