import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {TextUtils} from '@themost/client';

@Injectable()
export class ProfileService {
    constructor(private context: AngularDataContext) {

    }

    getStudent(): any {
        const sessionStudent = sessionStorage.getItem('student');
        if (sessionStudent != null) {
            return new Promise((resolve, reject) => {
                try {
                    // deserialize student data
                    const res = JSON.parse(sessionStudent, function (key, value) {
                        // parse dates
                        if (TextUtils.isDate(value)) {
                            return new Date(value);
                        }
                        // otherwise return value
                        return value;
                    });
                    // set a small delay
                    setTimeout(() => {
                        // and return result
                        return resolve(res);
                    }, 500);
                } catch (err) {
                    return reject(err);
                }
            });
        }
        return this.context.model('students/me')
            .asQueryable()
            .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'studentSeries', 'person($expand=gender)')
            .getItem().then(student => {
                if (typeof student === 'undefined') {
                    return Promise.reject('Student data cannot be found');
                }
                return this.context.model('LocalDepartments')
                    .asQueryable()
                    .where('id').equal(student.department.id)
                    .expand('organization($expand=instituteConfiguration,registrationPeriods)')
                    .getItem().then(department => {
                        if (typeof department === 'undefined') {
                            return Promise.reject('Student department cannot be found');
                        }
                        student.department = department;
                        sessionStorage.setItem('student', JSON.stringify(student));
                        return Promise.resolve(student);
                    });
            });
    }

}
