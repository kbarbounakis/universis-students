import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GraduationModule } from '@universis/graduation';

@NgModule({
    imports: [
        RouterModule,
        GraduationModule
    ]
})
export class GraduationImporterModule {
}