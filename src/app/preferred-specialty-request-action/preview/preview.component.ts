import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { ProfileService } from '../../profile/services/profile.service';


@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html'
})
export class PreviewComponent implements OnInit {

  public loading = true;
  public preferredSpecialtyRequestAction: any;

  constructor(private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _profileService: ProfileService) { }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  async ngOnInit() {
    try {
      this.showLoading(true);
      const student = await this._profileService.getStudent();
      const currentYear = student && student.department && student.department.currentYear && student.department.currentYear.id;
      const currentPeriod = student && student.department && student.department.currentPeriod && student.department.currentPeriod.id;
      this.preferredSpecialtyRequestAction = await this._context
        .model('PreferredSpecialtyRequestActions')
        .where('academicYear')
        .equal(currentYear)
        .and('academicPeriod')
        .equal(currentPeriod)
        .and('student')
        .equal(student && student.id)
        .expand('specialty')
        .orderByDescending('dateCreated')
        .getItem();
      this.showLoading(false);
    } catch (err) {
      console.error(err);
      this._errorService.navigateToError(err);
    } finally {
      this.showLoading(false);
    }
  }
}
