import { TestBed } from '@angular/core/testing';

import { BookDeliveriesService } from './book-deliveries.service';

describe('BookDeliveriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookDeliveriesService = TestBed.get(BookDeliveriesService);
    expect(service).toBeTruthy();
  });
});
