import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../../../profile/services/profile.service';
import { BookDeliveriesService } from '../../services/book-deliveries.service';

@Component({
    selector: 'app-registrations-home',
    templateUrl: './registrations-home.component.html',
    styleUrls: ['./registrations-home.component.scss']
})

export class RegistrationsHomeComponent implements OnInit {
    public useStudentRegisterAction = true;
    public isLoading = true;
    public supportedBookDeliveriesService: boolean = false;
    constructor(private _profileService: ProfileService, 
        private _bookDeliveriesService: BookDeliveriesService) { }

    async ngOnInit() {
        // get student
        const student = await this._profileService.getStudent();
        if (student) {
            // set use register action flag
            this.useStudentRegisterAction = student &&
                student.department &&
                student.department.organization &&
                student.department.organization.instituteConfiguration &&
                student.department.organization.instituteConfiguration.useStudentRegisterAction;


        }
        // check for books service
        this.supportedBookDeliveriesService = await this._bookDeliveriesService.supported();
        this.isLoading = false;
    }

}
