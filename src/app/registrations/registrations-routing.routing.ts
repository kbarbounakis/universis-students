import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationsHomeComponent } from './components/registrations-home/registrations-home.component';
import { RegistrationSemesterComponent } from './components/registrations-semester/registrations-semester.component';
import { RegistrationCoursesComponent } from './components/registrations-courses/registrations-courses.component';
import { RegistrationListComponent } from './components/registrations-list/registrations-list.component';
import { AuthGuard } from '@universis/common';
import {RegistrationCheckoutComponent} from './components/registration-checkout/registration-checkout.component';
import { RegistrationSpecialtyComponent } from './components/registration-specialty/registration-specialty.component';
import {RegistrationCheckoutCanDeactivateGuard} from './services/registration-checkout-can-deactivate-guard';
import {RegistrationCoursesCanDeactivateGuard} from './services/registration-courses-can-deactivate-guard';
import { RegistrationBooksComponent } from './components/registration-books/registration-books.component';

const routes: Routes = [
    {
        path: '',
        component: RegistrationsHomeComponent,
        canActivate: [
            AuthGuard
        ],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'semester',
            },
            {
                path: 'semester',
                component: RegistrationSemesterComponent
            },
            {
                path: 'courses',
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'overview',
                    },
                    {
                        path: 'overview',
                        component: RegistrationCoursesComponent,
                      canDeactivate: [RegistrationCoursesCanDeactivateGuard]
                    },
                    {
                        path: 'checkout',
                        component: RegistrationCheckoutComponent,
                        canDeactivate: [RegistrationCheckoutCanDeactivateGuard]
                    }
                ]
            },
            {
                path: 'specialty',
                component: RegistrationSpecialtyComponent
            },
            {
                path: 'list',
                component: RegistrationListComponent
            },
            {
                path: 'books',
                component: RegistrationBooksComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationsRoutingModule {
}
