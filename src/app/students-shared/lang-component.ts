import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router, RouterState} from '@angular/router';
import { ConfigurationService } from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
//import 'rxjs/add/operator/filter';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-lang-msgbox',
    template: '<div></div>'
})
export class LangComponent implements OnInit {

    public previousUrl?: string;

    constructor(private _config: ConfigurationService,
                private _translate: TranslateService,
                private _router: Router,
                private activatedRoute: ActivatedRoute) {
        this._router.events.pipe(
            filter(event => event instanceof NavigationEnd))
            .subscribe( (ev: any) => {
                this.previousUrl = ev.url;
            });
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            localStorage.setItem('currentLang', params['id']);
          this._translate.use(params['id']); // changes the translation
          if (params['route'] !== 'dashboard') {
            if (params['subroute']) {
              if (params['sub']) {
                this._router.navigate([(params['route'].toString()).concat('/').concat(params['subroute'].toString()).
                concat('/').concat(params['sub'].toString())]);
              } else {
                this._router.navigate([(params['route'].toString()).concat('/').concat(params['subroute'].toString())]);
              }
            } else {
              this._router.navigate([params['route'].toString()]);
            }
          } else {
            this._router.navigate(['/']);
          }
        });
    }


}
