import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable()
export class MessageSharedService {
  private subject = new Subject<any>();

  callUnReadMessages() {
    this.subject.next({  });
  }

  getAction(): Observable<any> {
    return this.subject.asObservable();
  }
}
