import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { ApplicationSettingsConfiguration, SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
//import { NgSpinKitModule } from 'ng-spin-kit';
import { RouterModule } from '@angular/router';
import { LangComponent } from './lang-component';
import { AppSidebarService } from './services/app-sidebar.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import * as el from "../../assets/i18n/el.json"
import * as en from "../../assets/i18n/en.json"




export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  useDigitalSignature: boolean;
  navigationLinks?: INavigationLinkConfiguration;
  messages?: any;
  title?: any;
  header?: Array<any>;
}

export declare interface INavigationLinkConfiguration {
  name: string;
  url: string;
  inLanguage: string;
  target: '_self' | '_blank' | '_parent' | '_top' | string;
}

export declare interface IRequestTypesConfiguration {
  name: string;
  alternateName: string;
  category: string;
  entryPoint: string;
  description?: string;
  inLanguage?: string;
  additionalType?: string;
  url?: string;
}

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    BsDropdownModule,
    ModalModule,
    //NgSpinKitModule,
    TooltipModule,
    RouterModule
  ],
  declarations: [
    LangComponent
  ],
  exports: [
    LangComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class StudentsSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: StudentsSharedModule,
    private _translateService: TranslateService) {
    if (parentModule) {
      // throw new Error(
      //    'StudentsSharedModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders<StudentsSharedModule> {
    return {
      ngModule: StudentsSharedModule,
      providers: [
        AppSidebarService
      ]
    };
  }

  async ngOnInit() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }

}
