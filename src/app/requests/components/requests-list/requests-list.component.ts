import { Component, OnInit } from '@angular/core';
import { RequestsService} from '../../services/requests.service';
import {AngularDataContext} from '@themost/angular';
import { LoadingService, ErrorService } from '@universis/common';
import {ModalService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import  {BsModalRef}  from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { ConfigurationService } from '@universis/common';
import { ApplicationSettings } from '../../../students-shared/students-shared.module';
import { RequestTypeItem as RequestTypeItemBase } from '@universis/common';

interface RequestTypeItem extends RequestTypeItemBase {
  additionalType?: string;
}

interface RequestCategory {
  title: string;
  entries: Array<RequestTypeItem>;
}

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss'],
  providers: [ RequestsService ]
})

export class RequestsListComponent implements OnInit {


  public documentTypes: any = [];
  public requests: any = [];
  public index_arr: any;
  public isCollapsed?: boolean[];
  public requestHeader;
  public loading = true;

  public messages: any;
  public selectedRequests:string  = "";
  public searchrequests: any = [];
  public filterdrequests: any;
  public initialRequests: any;
  public skip = 0;
  public take = 5;
  public count: any;
  public searchText:string = "";
  selector = '.inner__content';

  private requestsDialog?: BsModalRef;

  public requestTypes?: Array<RequestTypeItem>;
  public requestCategories?: Array<RequestCategory>;

  constructor(
    private _router: Router,
    private _context: AngularDataContext,
    private translate: TranslateService,
    private loadingService: LoadingService,
    private requestsService: RequestsService,
    private modalService: ModalService,
    private _configurationService: ConfigurationService,
    private _errorService: ErrorService
  ) {

  }

  /**
   *
   * Steps to initialize the requests component:
   *
   */
  async ngOnInit() {
     // show loading
    this.loading = true;
    this.loadingService.showLoading();

    // get header request message from app.json
    const messages = this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).messages;
    if (Array.isArray(messages)) {
      const message = messages.find(x => {
        return x.additionalType === 'StudentRequestHeader' && x.inLanguage === this._configurationService.currentLocale;
      });
      if (message) {
        this.requestHeader = message.body;
      }
    }

    try {
      this.requestTypes = await this.requestsService.getRequestTypes();
      this.documentTypes = await this.requestsService.getDocumentTypes();
    } catch (e) {
      this.requestTypes = [];
      this.documentTypes = [];
    }
    this.requestsService.getStudentRequests().then((items) => {
      items.filter((item) => {
        return item.additionalType === 'RequestDocumentAction' ;
      }).forEach ((item) => {
        item.object = this.documentTypes.find ((x) => {
          return x.id === item.object;
        });
      });
      items.forEach((item) => {
        item.name = item.name || this.translate.instant(`NewRequestTemplates.${item.additionalType}.Name`);
        item.requestType = this.requestTypes?.find((x) => {
            return x.additionalType === item.additionalType;
          });
      });
      this.requests = items;
      this.index_arr = this.requests;
      this.isCollapsed = Array(this.index_arr.length).fill(false);
      this.count = this.index_arr.length;
      this.initialRequests = this.requests;
      this.selectedRequests = 'All';
      this.getRequests(this.selectedRequests, this.skip, this.take);
      // hide loading
      this.loadingService.hideLoading();
      this.loading = false;
    }).catch( err => {
      // hide loading
      this.loadingService.hideLoading();
      this.loading = false;
      return this._errorService.navigateToError(err);
    });

  }

  /**
   *
   * Navigates to the given url
   *
   * @param {string} entryPoint The url to navigate to
   * @param {string} type The doctype of the request
   *
   */
  navigateToEntrypoint(entryPoint: string, type: string = '') {
    this.exitRequests();
    this._router.navigate([entryPoint], {queryParams: {type}});
  }

  /**
   *
   * Navigates to the given url with code as a query parameter
   *
   * @param {string} entryPoint The url to navigate to
   * @param {string} code The code of the request action
   *
   */
  navigateToEntrypointWithCode(entryPoint: string, code: string) {
    this._router.navigate([entryPoint], {queryParams: {code}});
  }

  // download the document file
  downloadFile(attachments) {
    this.requestsService.downloadFile(attachments);
  }

  sendRequest(title: string) {
    return this._context.model('RequestDocumentActions').save({
      object: {
        alternateName: title
      },
      // description: description,
    })
      .then(() => {
        window.location.href = ('/#/requests/list');
        window.location.reload();
      }).catch((err) => {
        this.exitRequests();
      });
  }

  /**
   *
   * Shows the error modal with information for a specific request
   *
   * @param {stirng} request The request name as it's written at the translation files.
   *
   */
  showError(request: string): void {
    this.modalService.showDialog(
      this.translate.instant('Requests.RequestFor')  + ' ' + this.translate.instant('NewRequestTemplates.' + request + '.Name'),
      this.translate.instant('Requests.AlreadyExists')
    );
    this.exitRequests();
  }

  newRequestDialog(ref) {
    this.requestsDialog = this.modalService.openModal(ref, 'modal--full');
  }

  exitRequests() {
    this.requestsDialog?.hide();
  }

  getRequests(selected: any, skip: any, take: any) {
    if (selected !== 'All') {
      this.searchText = '';
    }
    if (this.searchText.length === 0) {
      this.requests = skip === 0 ? [] : this.requests;
      this.skip = skip === 0 ? 0 : this.skip;
      if (selected === 'pending') {
        // show loading
        this.loadingService.showLoading();
        this.filterdrequests = this.initialRequests.filter((x) => {
          return x.actionStatus.alternateName === 'ActiveActionStatus';
        });
        this.count = this.filterdrequests.length;
        this.requests = this.requests.concat(this.filterdrequests.slice(skip, skip + take));
        // hide loading
        this.loadingService.hideLoading();
      } else if (selected === 'failed') {
        // show loading
        this.filterdrequests = this.initialRequests.filter((x) => {
          return x.actionStatus.alternateName === 'CancelledActionStatus' || x.actionStatus.alternateName === 'FailedActionStatus';
        });
        this.count = this.filterdrequests.length;
        this.requests = this.requests.concat(this.filterdrequests.slice(skip, skip + take));
        // hide loading
      } else if (selected === 'approved') {
        this.filterdrequests = this.initialRequests.filter((x) => {
          return x.actionStatus.alternateName === 'CompletedActionStatus';
        });
        this.count = this.filterdrequests.length;
        this.requests = this.requests.concat(this.filterdrequests.slice(skip, skip + take));
      } else {
        this.count = this.initialRequests.length;
        this.requests = this.requests.concat(this.initialRequests.slice(skip, skip + take));
      }
    }
  }

  onScroll() {
    if (this.count > (this.skip + this.take)) {
      this.skip += this.take;
      if (this.count > (this.skip + this.take)) {
        this.getRequests(this.selectedRequests, this.skip, this.take);
      } else {
        this.take = this.count - this.skip;
        this.getRequests(this.selectedRequests, this.skip, this.take);
      }
      this.take = 5;
    }
  }

  onSearchTextKeyDown($event: any) {
    if ($event.keyCode === 13) {
      this.searchrequests =  this.initialRequests.filter((x) => {
        return (x.object && x.object.name && x.object.name.toUpperCase().includes(this.searchText.toUpperCase())) ||
          (x.object && x.object.description && x.object.description.toUpperCase().includes(this.searchText.toUpperCase())) ||
          (x.name && x.name.toUpperCase().includes(this.searchText.toUpperCase())) ||
          (x.description && x.description.toUpperCase().includes(this.searchText.toUpperCase()));
      });
      this.selectedRequests = 'All';
      this.skip = 0;
      this.loadingService.showLoading();
      this.requests = this.searchrequests;
      this.index_arr = this.requests;
      this.isCollapsed = Array(this.index_arr.length).fill(false);
      this.count = this.index_arr.length;
      this.requests.sort((a, b) => {
        return b.dateCreated - a.dateCreated;
      });
      // hide loading
      this.loadingService.hideLoading();
    }
  }

  onSearchTextKeyUp($event: any) {
    if ($event.target && $event.target.value.length === 0) {
      this.selectedRequests = 'All';
      this.getRequests( this.selectedRequests, this.skip, this.take );
    }
  }

  open(url: string) {
    window.open(url, '_blank');
  }
}

