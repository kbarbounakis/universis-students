import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '@universis/common';
import { Title } from '@angular/platform-browser';
import { ApplicationSettings } from './students-shared/students-shared.module';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
    title = 'app';

    public constructor( private _config: ConfigurationService,
                        private _titleService: Title ) {}

    public setTitle( newTitle: string) {
      this._titleService.setTitle( newTitle );
    }

    async ngOnInit() {
      const customTitle = (<ApplicationSettings>this._config.settings.app) && (<ApplicationSettings>this._config.settings.app).title; 
  
      if (customTitle && customTitle.trim() !== '') {
        this.setTitle(customTitle);
      }
    }

}
